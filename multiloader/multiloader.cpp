#include "pxdip.h"
#include "cpudetect.h"
#include <dlfcn.h>
#include <string>
#include <unistd.h>

using namespace std;
using namespace multiloader;

#define xstr(s) str(s)
#define str(s) #s

//original shared library's functions
namespace origfn {
    typedef unsigned int (*ver_fn_type)(void);
    typedef const char* (*ver_str_fn_type)(void);
    typedef void* (*init_fn_type)(const uint8_t);
    typedef void (*destroy_fn_type)(const void*);
    typedef void (*setmask_fn_type)(const void*, const uint32_t, uint32_t* const, const uint32_t);
    typedef void (*raa_fn_type)(const void*, const uint16_t, const uint16_t, const uint16_t, const uint16_t, const uint16_t, const uint16_t, uint32_t* const, uint32_t* const);

    ver_fn_type pxdip_api_version;
    ver_fn_type pxdip_revision;
    ver_str_fn_type pxdip_version_desc;
    init_fn_type pxdip_init;
    destroy_fn_type pxdip_destroy;
    setmask_fn_type pxdip_setmask;
    raa_fn_type pxdip_raa;
}

namespace priv {
    //private prototypes
    void* load_px_dip();
    string gen_ver_desc();

    //private variables
    void *src_hdl = load_px_dip();
    string ver_desc = gen_ver_desc();
}

void* priv::load_px_dip() {
    string libfile = xstr(LIB_PREFIX);
    if(libfile[libfile.size() - 1] != '/') {
        libfile += '/';
    }
    libfile += "lib/px_dip/";

    //optimisation runtime detection
    auto cpu_type = get_cpu_type();
    switch (cpu_type) {
        case OptimType::AVX:
            libfile += "libpx_dip-avx.so";
            break;
        case OptimType::AVX2:
            libfile += "libpx_dip-avx2.so";
            break;
        default:
            libfile += "libpx_dip-standard.so";
            break;
    }

    //load the library
    void* hdl = dlopen(libfile.c_str(), RTLD_LAZY);
    //if errors, we'll force exit the app no matter what.
    if(!hdl) {
        fputs(dlerror(), stderr);
        fputc('\n', stderr);
        _exit(1);
    }

    //populate "origfn"
    origfn::pxdip_api_version = (origfn::ver_fn_type)dlsym(hdl, "pxdip_api_version");
    origfn::pxdip_revision = (origfn::ver_fn_type)dlsym(hdl, "pxdip_revision");
    origfn::pxdip_version_desc = (origfn::ver_str_fn_type)dlsym(hdl, "pxdip_version_desc");
    origfn::pxdip_init = (origfn::init_fn_type)dlsym(hdl, "pxdip_init");
    origfn::pxdip_destroy = (origfn::destroy_fn_type)dlsym(hdl, "pxdip_destroy");
    origfn::pxdip_setmask = (origfn::setmask_fn_type)dlsym(hdl, "pxdip_setmask");
    origfn::pxdip_raa = (origfn::raa_fn_type)dlsym(hdl, "pxdip_raa");

    //check if all symbols are successfully loaded
    for(auto fp : {(void*)origfn::pxdip_api_version, (void*)origfn::pxdip_revision, (void*)origfn::pxdip_version_desc,
                   (void*)origfn::pxdip_init, (void*)origfn::pxdip_destroy, (void*)origfn::pxdip_setmask,
                   (void*)origfn::pxdip_raa}) {
        if(fp == nullptr) {
            fputs("Cannot load some symbols from px_dip\n", stderr);
            _exit(1);
        }
    }

    //also check the loaded library API version
    auto orig_api_ver = origfn::pxdip_api_version();
    if(orig_api_ver != PXDIP_API) {
        fprintf(stderr, "API version mismatch. Expected %d, got %d.", PXDIP_API, orig_api_ver);
        _exit(1);
    }

    //OK!
    return hdl;
}

string priv::gen_ver_desc() {
    return "multiloader - " + string(origfn::pxdip_version_desc());
}

unsigned int pxdip_api_version() {
    return PXDIP_API;
}

unsigned int pxdip_revision() {
    return origfn::pxdip_revision();
}

const char* pxdip_version_desc() {
    return priv::ver_desc.c_str();
}

void* pxdip_init(const uint8_t max_thread) {
    return origfn::pxdip_init(max_thread);
}

void pxdip_destroy(const void* inst) {
    origfn::pxdip_destroy(inst);
}

void pxdip_setmask(const void* inst, const uint32_t count, uint32_t* const data, const uint32_t mask) {
    origfn::pxdip_setmask(inst, count, data, mask);
}

void pxdip_raa(const void* inst,
               const uint16_t cp_x,
               const uint16_t cp_y,
               const uint16_t cp_w,
               const uint16_t cp_h,
               const uint16_t src_w,
               const uint16_t src_h,
               uint32_t* const src,
               uint32_t* const dst) {
    origfn::pxdip_raa(inst, cp_x, cp_y, cp_w, cp_h, src_w, src_h, src, dst);
}