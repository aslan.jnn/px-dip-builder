#pragma once

namespace multiloader {
    enum class OptimType {
        STANDARD, AVX, AVX2
    };

    OptimType get_cpu_type();
}