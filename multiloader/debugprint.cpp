#include "debugprint.h"
#include <iostream>

using namespace std;

namespace multiloader {
#ifdef NDEBUG
    bool dbgmsg = false;
#else
    bool dbgmsg = true;
#endif

    void dbgprint(std::string msg) {
        if (dbgmsg) {
            cout << msg << endl;
        }
    }
}