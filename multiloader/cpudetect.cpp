#include "cpudetect.h"
#include "debugprint.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <set>

using namespace std;
using namespace multiloader;

namespace priv {
    set<string> get_os_cpu_flags() {
        fstream fs;
        fs.open("/proc/cpuinfo", fstream::in);
        if(fs.fail()) {
            dbgprint("Cannot open '/proc/cpuinfo'.");
            return {};
        }

        set<string> ret;

        string line;
        while (!fs.eof()) {
            getline(fs, line);
            //case insensitive
            transform(line.begin(), line.end(), line.begin(), ::tolower);
            //tokenize
            istringstream iss(line);
            string token;
            //check if iss has some token(s)
            if(iss) {
                iss >> token;
                if(token == "flags") {
                    //colon
                    iss >> token;
                    //returned CPU flags
                    while (iss) {
                        iss >> token;
                        ret.insert(token);
                    }
                    break;
                }
            }
        }

        fs.close();
        return ret;
    }
}

OptimType multiloader::get_cpu_type() {
    auto flags = priv::get_os_cpu_flags();
    if(!flags.size()) {
        dbgprint("Cannot retrieve CPU flags.");
    }
    //order the "switch case" so that "most optimized" version gets checked first
    if(flags.count("avx2")) {
        return OptimType::AVX2;
    } else if (flags.count("avx")) {
        return OptimType::AVX;
    }
    //otherwise ...
    return OptimType::STANDARD;
};