cmake_minimum_required(VERSION 3.4)
project(px-dip)

set(CMAKE_BUILD_TYPE Release FORCE)
set(ADDITIONAL_COMPILE_OPTIONS -Ofast)

add_subdirectory(../../px-dip build)

set_target_properties(px_dip PROPERTIES LIBRARY_OUTPUT_NAME "px_dip-standard")